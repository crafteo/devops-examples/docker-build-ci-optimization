# Docker build optimization for local build and GitLab CI

A few examples of Docker build optimization. This small project contains GitLab runner configuration each used to apply a CI optimization pattern. 

This project is part of the [Devops Examples collection](https://github.com/PierreBeucher/devops-examples)

## Local build

```sh
# Basic build without optimization
docker build -f Dockerfile.nooptim -t optim:local .

# Simple build cache optimization
docker build -f Dockerfile -t optim:local .

# BuildKit with cache mount and pnpm
docker buildx build -f Dockerfile.buildkit_and_pnpm . --load -t optim:local
```

Run with

```
docker run -d -p 3000:3000 optim:local
```

## CI build with GitLab CI

### Setup

Requirements:

- Docker 19.03+
- Docker Compose 1.27+
- Vagrant 2.2+ 
- VirtualBox 6.1+

Summary:

- Gitlab Runner is deployed as Docker container locally. You must provide a registration token during setup.
- A Vagrant (VirtualBox) machine hosts our Docker daemon. 

Run:

```
./setup.sh
```

You'll be prompted for a [Gitlab CI registration token](https://docs.gitlab.com/ee/ci/runners/configure_runners.html#reset-the-runner-registration-token-for-a-project). Create one for the project.

Runners should now appear in Gitlab. 

[Docker daemon over SSH](https://docs.docker.com/engine/security/protect-access/#use-ssh-to-protect-the-docker-daemon-socket) should now be available via `ssh://vagrant@192.168.56.2`. For example:

```
docker -H ssh://vagrant@192.168.56.2 info
```

Cleanup when done:

```
./teardown.sh
```

## Example CI jobs

### Simple Docker in Docker

Runner: `Simple Docker in Docker`
Job: `dind-simple` and `dind-pull-before`

Using Docker in Docker on CI, simply pull Docker image and use its cache before building.

### Share Docker daemon with underlying host to re-use cache

Runner: `Shared host Docker daemon`
Job: `shared-dockerd`

Share underlying host Docker daemon so that CI jobs always use the same daemon: no need for DinD and easy cache re-use on runner!

### Remote Docker daemon shared between multiple runners

Runner: `Remote secured Docker daemon`
Job: `remote-dockerd`

Share a remotely accessible (and secured!) Docker daemon between runners. No need for DinD and easy cache re-use with multiple runners!

### Save build cache locally for your build tool (example with `pnpm`)

Runner: `Remote secured Docker daemon` or `Shared host Docker daemon`
Job: `buildkit-pnpm-cache`

Optimize your tooling to keep your favourire build tool cache (such as pnpm or Maven) in a Docker volume

---


## Docker Daemon with TLS

Running remote secured Docker daemon requires generation of client certificate (for Docker client) and server (for Docker daemon) certificates:

```
cd secure-dockerd
```

```
# Generate certificates in .tls/
./generate-docker-certs.sh
```

You can either use a secured Docker daemon runnin in container (default) or a remote Daemon on AWS EC2 instance.

Docker daemon container with Docker Compose:

```
docker-compose up -d

# Test secured daemon
unset DOCKER_HOST DOCKER_CERT_PATH DOCKER_TLS
export DOCKER_HOST=$(docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' dockerd_tls):2376
export DOCKER_CERT_PATH=$PWD/.tls/client
export DOCKER_TLS=1
docker ps
```

Remote Daemon on AWS EC2 instance using Ansible playbook:

```
ansible-playbook playbook.yml

# Test secured daemon (instance IP is provided by playbook after deploy)
unset DOCKER_HOST DOCKER_CERT_PATH DOCKER_TLS
export DOCKER_HOST=tcp://x.y.z.z:2376
export DOCKER_CERT_PATH=$PWD/.tls/client
export DOCKER_TLS=1
```

Cleanup:

```
ansible-playbook playbook.yml -e docker_state=absent
```