#!/bin/bash

# Register a runner using Gitlab API and replace token variable in Runner config file
# $1: description
# $2: runner tag (and variable in template)
# $3: registration token
registerRunnerAndTemplateConfig ()
{
    echo "Registering Gitlab runner $1..."

    runner_token=$(curl -s --request POST https://gitlab.com/api/v4/runners \
      --form "token=$3" \
      --form "description=$1" \
      --form "tag_list=$2" \
      --form "run_untagged=false" | jq -r .token)

    sed -i "s/\${$2}/$runner_token/g" gitlab-runner-config.tmp.toml
}

if [ -f "gitlab-runner-config.toml" ]; then
    echo "Gitlab runner config gitlab-runner-config.toml already exists. Restart runner registration? (will override existing config) y/N"
    read do_register

    if [ "$do_register" != "y" ] && [ "$do_register" != "Y" ]; then
      echo "Skipping Gitlab runner registration..."
      exit 0
    fi
fi

if [ -z ${REGISTRATION_TOKEN+x} ]; then 
  echo "Please enter Gitlab registration token (see project config > CI/CD > Runners) (or set REGISTRATION_TOKEN and re-run script)"
  read -s regtoken
else 
  regtoken=${REGISTRATION_TOKEN}
fi

# Copy template to tmp file and find/replace config tokens
cp gitlab-runner-config.template.toml gitlab-runner-config.tmp.toml

registerRunnerAndTemplateConfig "Simple Docker in Docker" "dind-simple" $regtoken
registerRunnerAndTemplateConfig "Shared host Docker daemon" "shared-host-dockerd" $regtoken
registerRunnerAndTemplateConfig "Remote TLS Docker daemon" "remote-tls-dockerd" $regtoken
registerRunnerAndTemplateConfig "Remote SSH Docker daemon" "remote-ssh-dockerd" $regtoken

# replace PWD be able to mount directories from within Gitlab Runner config
sed -i 's?${PWD}?'`pwd`'?' gitlab-runner-config.tmp.toml

cp gitlab-runner-config.tmp.toml gitlab-runner-config.toml
rm gitlab-runner-config.tmp.toml

echo "Gitlab Runner config ready: gitlab-runner-config.toml"
