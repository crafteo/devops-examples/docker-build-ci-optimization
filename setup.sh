#!/bin/bash

echo "Setting up Gitlab runner config..."

./register-runners.sh

echo "Setting-up Docker daemon with Vagrant..."

echo "Generating SSH key for VM"
ssh-keygen -t rsa -f secure-dockerd/.ssh/key -q -N ""

echo "Setting-up Vagrant VM"

# Reset SSH host key for Vagrant VM
ssh-keygen -f "$HOME/.ssh/known_hosts" -R 192.168.56.2
# ssh-keyscan -H 192.168.56.2 >> ~/.ssh/known_hosts
sh -c "cd secure-dockerd && vagrant up --provision && ansible-playbook playbook-docker-classic.yml"

echo "Starting Gitlab Runner..."

docker-compose up -d