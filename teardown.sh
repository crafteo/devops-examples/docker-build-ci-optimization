#!/bin/bash

echo "Stopping secured Docker daemon and Gitlab runner..."

docker-compose down -v

echo "Stopping Vagrant VM..."

sh -c "cd secure-dockerd && vagrant destroy -f"