# Dockerfile with basic optimizations
# - Use alpine image
# - Reduce cache invalidation by only 
#   copying files required to run
#   deps installation
#
# Outside of Dockerfile:
# - Use .dockerignore to reduce context size 
FROM node:17-alpine
RUN mkdir /app
WORKDIR /app

# Copy package and lock file to avoid cache invalidation
# If other files are modified
COPY package.json package-lock.json /app/

RUN npm i --ignore-script

# Copy remaining files after dependencies install
COPY index.js /app/index.js

CMD [ "node", "index.js"]